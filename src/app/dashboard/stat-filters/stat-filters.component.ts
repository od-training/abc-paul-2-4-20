import { Component } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.scss']
})
export class StatFiltersComponent {
  searchControl: FormControl = this.appService.searchControl;
  constructor(private appService: AppService) {}
}
