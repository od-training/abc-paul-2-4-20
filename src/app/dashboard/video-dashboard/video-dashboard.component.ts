import { Component } from '@angular/core';

import { AppService } from '../../app.service';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss']
})
export class VideoDashboardComponent {
  videoList = this.appService.videoList;
  selectedVideo = this.appService.selectedVideo;

  constructor(private appService: AppService) {}
}
