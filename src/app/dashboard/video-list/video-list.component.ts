import { Component, Input } from '@angular/core';
import { Video } from '../../app.types';
import { Router } from '@angular/router';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent {
  @Input() videosList: Video[] = [];
  @Input() selectedVideo: Video | undefined;

  constructor(private router: Router) {}

  selectVideo(video: Video) {
    this.router.navigate([], { queryParams: { selected: video.id } });
  }
}
