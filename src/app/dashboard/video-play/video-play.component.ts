import { Component, Input } from '@angular/core';
import { Video } from '../../app.types';

@Component({
  selector: 'app-video-play',
  templateUrl: './video-play.component.html',
  styleUrls: ['./video-play.component.scss']
})
export class VideoPlayComponent {
  @Input() selectedVideo: Video | undefined;
}
