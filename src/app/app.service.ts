import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Video } from './app.types';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, combineLatest } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  readonly videos = this.httpClient.get<Video[]>(
    'https://api.angularbootcamp.com/videos'
  );
  readonly searchControl = new FormControl();
  readonly videoList: Observable<Video[]>;
  readonly selectedVideo: Observable<Video>;
  private searchTerm: Observable<string>;
  private requestedVideoId: Observable<string>;

  constructor(
    private httpClient: HttpClient,
    router: Router,
    activatedRoute: ActivatedRoute
  ) {
    this.searchControl.valueChanges.subscribe(searchTerm =>
      router.navigate([], { queryParams: { filter: searchTerm } })
    );

    this.searchTerm = activatedRoute.queryParamMap.pipe(
      map(params => params.get('filter') || '')
    );

    this.videoList = combineLatest([this.videos, this.searchTerm]).pipe(
      map(([videos, searchTerm]) =>
        videos.filter(video => video.title.includes(searchTerm))
      ),
      shareReplay(1)
    );

    this.requestedVideoId = activatedRoute.queryParamMap.pipe(
      map(params => params.get('selected') || '')
    );

    this.selectedVideo = combineLatest([
      this.videoList,
      this.requestedVideoId
    ]).pipe(
      map(([videoList, requestedVideoId]) => {
        const selected = videoList.find(video => video.id === requestedVideoId);
        if (selected) {
          return selected;
        } else {
          return videoList[0];
        }
      }),
      shareReplay(1)
    );
  }
}
